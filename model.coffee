@Documents = new Meteor.Collection("documents");

Meteor.methods
  deleteDocument: (id) ->
    Documents.remove(id)
  createReceipt: (doc_id, InkBlobs) ->
    _.each(InkBlobs, (InkBlob) ->
      Documents.update({_id: doc_id}, {$push: { receipts:
        name: "un-named"
        desc: ""
        filename: InkBlob.filename
        mimetype: InkBlob.mimetype
        size: InkBlob.size
        url: InkBlob.url}}
      )
    )
    