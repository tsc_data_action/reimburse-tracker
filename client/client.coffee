Functional.install()
fp_api_key = "AcMqRoWwR1KkoJDy88ELhz"

Handlebars.registerHelper "withif", (obj, options) ->
  if obj then options.fn(obj) else options.inverse(this)

Template.docList.documents = -> Documents.find()

Template.docList.events =
  "click button": ->
    Documents.insert 
      title: "untitled"
      purpose: "undefined"
      receipts: [], 
      (err, id) ->
        return unless id
        Session.set("document", id)

Template.docItem.current = -> Session.equals("document", @_id)

Template.docItem.events =
  "click a": (e) ->
    e.preventDefault()
    Session.set("document", @_id)

# Strange bug https://github.com/meteor/meteor/issues/1447
Template.docTitle.title = -> Documents.findOne(@substring 0)?.title

Template.editor.docid = -> Session.get("document")

Template.editor.events =
  "keydown input.name ": (e) ->
    return unless e.keyCode == 13
    e.preventDefault()

    $(e.target).blur()
    id = Session.get("document")
    Documents.update id, {$set: {title: e.target.value}}

  "click button.delete": (e) ->
    e.preventDefault()
    id = Session.get("document") 
    if confirm("You sure you want to delete?")
      Session.set("document", null)
      Meteor.call "deleteDocument", id

Template.entry.rendered = ->
  loadPicker(fp_api_key, () ->
    filepicker.makeDropPane(
      $('#dropzone')[0],
      {
        multiple: false
        dragEnter: () ->
          $('#dropzone')
            .html("Drop to upload map")
            .css({
              'text-align': 'center'
              'background-color': '#ffffff'
              'color': '#ddd'
              'border': '2px dashed #ddd'
              'height': '75px';
              'line-height': '75px';
              'margin-bottom': '20px';
            })
          null
        dragLeave: () ->
          $('#dropzone')
            .html("Drag and Drop map images to create new maps")
            .css({
              'text-align': 'center'
              'background-color': '#ffffff'
              'color': '#ddd'
              'border': '2px dashed #ddd'
              'height': '75px';
              'line-height': '75px';
              'margin-bottom': '20px';
            })
          null
        onSuccess: (InkBlobs) ->
          $('#dropzone').text("Done!")
          Meteor.call('createReceipt', Session.get("document"), InkBlobs)
        onProgress: (percentage) ->
          $("#dropzone")
            .text("Uploading ("+percentage+"%)")
          null
      }
    )
  )
  
  $(this.find(".sidebar .purpose:not(.editable)"))
    .editable('destroy')
    .editable(success: updateEntryField("purpose",I))
  $(this.find(".sidebar .desc:not(.editable)"))
    .editable('destroy')
    .editable(success: updateEntryField("desc",I))
  $(this.find(".sidebar .bpurpose:not(.editable)"))
    .editable('destroy')
    .editable(success: updateEntryField("bpurpose",I))
    
Template.entry.currentDoc = -> Documents.findOne({_id: Session.get("document")})
Template.entry.currentReceipts = -> 
  a = Documents.findOne({_id: Session.get("document")})
  if a?
    return a.receipts
  return undefined

updateEntryField = ((field, parse, response, newValue) ->
  console.log field, parse, response, newValue
  update = {}
  update[field] = parse(newValue)
  Documents.update(
    {"_id":Session.get("document")},
    {$set: update}
  )
  null).autoCurry()

